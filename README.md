# Univerter Desktop

## Warning: This project has barely started development. It is NOT currently functional.

A desktop downloader & converter for videos. The Web version can be found at https://univerter.dev

Currently supports a wide variety of formats and sites, but if you have any other formats you want supported, please open an issue.
Support for sites will only be added if those sites are supported by yt-dlp.